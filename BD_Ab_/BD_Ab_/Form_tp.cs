﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD_Ab_
{
    public partial class Form_tp : Form
    {
        public Form_tp()
        {
            InitializeComponent();
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
        public Dictionary<int, string> PhoneData
        {
            set
            {
                comboBox_phone.DataSource = value.ToArray();
                comboBox_phone.DisplayMember = "value";
            }
        }
        public Dictionary<int, string> FioData
        {
            set
            {
                comboBox_fio.DataSource = value.ToArray();
                comboBox_fio.DisplayMember = "value";
            }
        }
     
        
     

     
        public int FioId
        {
            get
            {
                return ((KeyValuePair<int, string>)comboBox_fio.SelectedItem).Key;
            }
            set
            {
                int idx = 0;
                foreach (KeyValuePair<int, string> item in comboBox_fio.Items)
                {
                    if (item.Key == value)
                    {
                        break;
                    }
                    idx++;
                }
                comboBox_fio.SelectedIndex = idx;
            }
        }
        
        public int PhoneId
        {
            get
            {
                return ((KeyValuePair<int, string>)comboBox_phone.SelectedItem).Key;
            }
            set
            {
                int idx = 0;
                foreach (KeyValuePair<int, string> item in comboBox_phone.Items)
                {
                    if (item.Key == value)
                    {
                        break;
                    }
                    idx++;
                }
                comboBox_phone.SelectedIndex = idx;
            }
        }
    }
}
