﻿namespace BD_Ab_
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_add_pr = new System.Windows.Forms.Button();
            this.button_delete_ab = new System.Windows.Forms.Button();
            this.button_red_ab = new System.Windows.Forms.Button();
            this.button_add_ab = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.ФИО = new System.Windows.Forms.Label();
            this.FIO_Text = new System.Windows.Forms.TextBox();
            this.find_phone = new System.Windows.Forms.TextBox();
            this.main_tab_control = new System.Windows.Forms.TabControl();
            this.name = new System.Windows.Forms.TabPage();
            this.abonent_t = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.contact_t = new System.Windows.Forms.DataGridView();
            this.provaider = new System.Windows.Forms.TabPage();
            this.provider_t = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ab_tab = new System.Windows.Forms.DataGridView();
            this.button_delete_pr = new System.Windows.Forms.Button();
            this.button_red_pr = new System.Windows.Forms.Button();
            this.button_add_cont = new System.Windows.Forms.Button();
            this.button_red_c = new System.Windows.Forms.Button();
            this.button_delete_c = new System.Windows.Forms.Button();
            this.button_red_tp = new System.Windows.Forms.Button();
            this.button_add_tp = new System.Windows.Forms.Button();
            this.button_delete_tp = new System.Windows.Forms.Button();
            this.main_tab_control.SuspendLayout();
            this.name.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.abonent_t)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contact_t)).BeginInit();
            this.provaider.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.provider_t)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ab_tab)).BeginInit();
            this.SuspendLayout();
            // 
            // button_add_pr
            // 
            this.button_add_pr.Location = new System.Drawing.Point(817, 260);
            this.button_add_pr.Name = "button_add_pr";
            this.button_add_pr.Size = new System.Drawing.Size(175, 47);
            this.button_add_pr.TabIndex = 0;
            this.button_add_pr.Text = "Добавить провайдера";
            this.button_add_pr.UseVisualStyleBackColor = true;
            this.button_add_pr.Click += new System.EventHandler(this.button_add_pr_Click);
            // 
            // button_delete_ab
            // 
            this.button_delete_ab.Location = new System.Drawing.Point(817, 152);
            this.button_delete_ab.Name = "button_delete_ab";
            this.button_delete_ab.Size = new System.Drawing.Size(175, 45);
            this.button_delete_ab.TabIndex = 24;
            this.button_delete_ab.Text = " Удалить абонента";
            this.button_delete_ab.UseVisualStyleBackColor = true;
            this.button_delete_ab.Click += new System.EventHandler(this.button_delete_ab_Click);
            // 
            // button_red_ab
            // 
            this.button_red_ab.Location = new System.Drawing.Point(817, 88);
            this.button_red_ab.Name = "button_red_ab";
            this.button_red_ab.Size = new System.Drawing.Size(175, 53);
            this.button_red_ab.TabIndex = 23;
            this.button_red_ab.Text = "Редактировать абонента";
            this.button_red_ab.UseVisualStyleBackColor = true;
            this.button_red_ab.Click += new System.EventHandler(this.button_red_ab_Click);
            // 
            // button_add_ab
            // 
            this.button_add_ab.Location = new System.Drawing.Point(817, 40);
            this.button_add_ab.Name = "button_add_ab";
            this.button_add_ab.Size = new System.Drawing.Size(175, 45);
            this.button_add_ab.TabIndex = 22;
            this.button_add_ab.Text = "Добавить абонента";
            this.button_add_ab.UseVisualStyleBackColor = true;
            this.button_add_ab.Click += new System.EventHandler(this.button_add_ab_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(814, 666);
            this.label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(121, 17);
            this.label.TabIndex = 21;
            this.label.Text = "Номер телефона";
            // 
            // ФИО
            // 
            this.ФИО.AutoSize = true;
            this.ФИО.Location = new System.Drawing.Point(844, 717);
            this.ФИО.Name = "ФИО";
            this.ФИО.Size = new System.Drawing.Size(42, 17);
            this.ФИО.TabIndex = 20;
            this.ФИО.Text = "ФИО";
            // 
            // FIO_Text
            // 
            this.FIO_Text.Location = new System.Drawing.Point(942, 714);
            this.FIO_Text.Name = "FIO_Text";
            this.FIO_Text.Size = new System.Drawing.Size(100, 22);
            this.FIO_Text.TabIndex = 19;
            this.FIO_Text.TextChanged += new System.EventHandler(this.FIO_Text_TextChanged);
            // 
            // find_phone
            // 
            this.find_phone.Location = new System.Drawing.Point(942, 666);
            this.find_phone.Name = "find_phone";
            this.find_phone.Size = new System.Drawing.Size(100, 22);
            this.find_phone.TabIndex = 18;
            this.find_phone.TextChanged += new System.EventHandler(this.find_phone_TextChanged);
            // 
            // main_tab_control
            // 
            this.main_tab_control.Controls.Add(this.name);
            this.main_tab_control.Controls.Add(this.tabPage1);
            this.main_tab_control.Controls.Add(this.provaider);
            this.main_tab_control.Controls.Add(this.tabPage2);
            this.main_tab_control.Location = new System.Drawing.Point(-3, 4);
            this.main_tab_control.Margin = new System.Windows.Forms.Padding(4);
            this.main_tab_control.Name = "main_tab_control";
            this.main_tab_control.SelectedIndex = 0;
            this.main_tab_control.Size = new System.Drawing.Size(779, 760);
            this.main_tab_control.TabIndex = 17;
            // 
            // name
            // 
            this.name.Controls.Add(this.abonent_t);
            this.name.Location = new System.Drawing.Point(4, 25);
            this.name.Margin = new System.Windows.Forms.Padding(4);
            this.name.Name = "name";
            this.name.Padding = new System.Windows.Forms.Padding(4);
            this.name.Size = new System.Drawing.Size(771, 731);
            this.name.TabIndex = 0;
            this.name.Text = "Абонент";
            this.name.UseVisualStyleBackColor = true;
            // 
            // abonent_t
            // 
            this.abonent_t.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.abonent_t.Location = new System.Drawing.Point(3, -5);
            this.abonent_t.Margin = new System.Windows.Forms.Padding(4);
            this.abonent_t.Name = "abonent_t";
            this.abonent_t.RowHeadersWidth = 51;
            this.abonent_t.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.abonent_t.Size = new System.Drawing.Size(777, 732);
            this.abonent_t.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.contact_t);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(771, 731);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "Контакт";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // contact_t
            // 
            this.contact_t.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.contact_t.Location = new System.Drawing.Point(0, 2);
            this.contact_t.Margin = new System.Windows.Forms.Padding(4);
            this.contact_t.Name = "contact_t";
            this.contact_t.RowHeadersWidth = 51;
            this.contact_t.Size = new System.Drawing.Size(772, 750);
            this.contact_t.TabIndex = 0;
            // 
            // provaider
            // 
            this.provaider.Controls.Add(this.provider_t);
            this.provaider.Location = new System.Drawing.Point(4, 25);
            this.provaider.Margin = new System.Windows.Forms.Padding(4);
            this.provaider.Name = "provaider";
            this.provaider.Size = new System.Drawing.Size(771, 731);
            this.provaider.TabIndex = 2;
            this.provaider.Text = "Провайдер";
            this.provaider.UseVisualStyleBackColor = true;
            // 
            // provider_t
            // 
            this.provider_t.AccessibleDescription = "";
            this.provider_t.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.provider_t.Location = new System.Drawing.Point(0, 2);
            this.provider_t.Margin = new System.Windows.Forms.Padding(4);
            this.provider_t.Name = "provider_t";
            this.provider_t.RowHeadersWidth = 51;
            this.provider_t.ShowCellToolTips = false;
            this.provider_t.Size = new System.Drawing.Size(780, 781);
            this.provider_t.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ab_tab);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(771, 731);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Телефонный справочник";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ab_tab
            // 
            this.ab_tab.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ab_tab.Location = new System.Drawing.Point(4, 4);
            this.ab_tab.Margin = new System.Windows.Forms.Padding(4);
            this.ab_tab.Name = "ab_tab";
            this.ab_tab.RowHeadersWidth = 51;
            this.ab_tab.Size = new System.Drawing.Size(772, 731);
            this.ab_tab.TabIndex = 0;
            // 
            // button_delete_pr
            // 
            this.button_delete_pr.Location = new System.Drawing.Point(817, 366);
            this.button_delete_pr.Name = "button_delete_pr";
            this.button_delete_pr.Size = new System.Drawing.Size(175, 72);
            this.button_delete_pr.TabIndex = 25;
            this.button_delete_pr.Text = "Удалить провайдера";
            this.button_delete_pr.UseVisualStyleBackColor = true;
            this.button_delete_pr.Click += new System.EventHandler(this.button_delete_pr_Click);
            // 
            // button_red_pr
            // 
            this.button_red_pr.Location = new System.Drawing.Point(817, 313);
            this.button_red_pr.Name = "button_red_pr";
            this.button_red_pr.Size = new System.Drawing.Size(175, 47);
            this.button_red_pr.TabIndex = 26;
            this.button_red_pr.Text = "Редактировать провайдера";
            this.button_red_pr.UseVisualStyleBackColor = true;
            this.button_red_pr.Click += new System.EventHandler(this.button_red_pr_Click);
            // 
            // button_add_cont
            // 
            this.button_add_cont.Location = new System.Drawing.Point(1019, 38);
            this.button_add_cont.Name = "button_add_cont";
            this.button_add_cont.Size = new System.Drawing.Size(175, 47);
            this.button_add_cont.TabIndex = 27;
            this.button_add_cont.Text = "Добавить контакт";
            this.button_add_cont.UseVisualStyleBackColor = true;
            this.button_add_cont.Click += new System.EventHandler(this.button_add_cont_Click);
            // 
            // button_red_c
            // 
            this.button_red_c.Location = new System.Drawing.Point(1019, 94);
            this.button_red_c.Name = "button_red_c";
            this.button_red_c.Size = new System.Drawing.Size(175, 47);
            this.button_red_c.TabIndex = 28;
            this.button_red_c.Text = "Редактировать контакт";
            this.button_red_c.UseVisualStyleBackColor = true;
            this.button_red_c.Click += new System.EventHandler(this.button_red_c_Click);
            // 
            // button_delete_c
            // 
            this.button_delete_c.Location = new System.Drawing.Point(1019, 152);
            this.button_delete_c.Name = "button_delete_c";
            this.button_delete_c.Size = new System.Drawing.Size(175, 47);
            this.button_delete_c.TabIndex = 29;
            this.button_delete_c.Text = "Удалить контакт";
            this.button_delete_c.UseVisualStyleBackColor = true;
            this.button_delete_c.Click += new System.EventHandler(this.button_delete_c_Click);
            // 
            // button_red_tp
            // 
            this.button_red_tp.Location = new System.Drawing.Point(1017, 313);
            this.button_red_tp.Name = "button_red_tp";
            this.button_red_tp.Size = new System.Drawing.Size(175, 47);
            this.button_red_tp.TabIndex = 30;
            this.button_red_tp.Text = "Редактировать контакт в телефонном справочнике";
            this.button_red_tp.UseVisualStyleBackColor = true;
            this.button_red_tp.Click += new System.EventHandler(this.button_red_tp_Click);
            // 
            // button_add_tp
            // 
            this.button_add_tp.Location = new System.Drawing.Point(1019, 260);
            this.button_add_tp.Name = "button_add_tp";
            this.button_add_tp.Size = new System.Drawing.Size(173, 47);
            this.button_add_tp.TabIndex = 31;
            this.button_add_tp.Text = "Добавить контакт в телефонный справочник";
            this.button_add_tp.UseVisualStyleBackColor = true;
            this.button_add_tp.Click += new System.EventHandler(this.button_add_tp_Click);
            // 
            // button_delete_tp
            // 
            this.button_delete_tp.Location = new System.Drawing.Point(1017, 366);
            this.button_delete_tp.Name = "button_delete_tp";
            this.button_delete_tp.Size = new System.Drawing.Size(175, 72);
            this.button_delete_tp.TabIndex = 32;
            this.button_delete_tp.Text = "Удалить контакт из телефонного справочника";
            this.button_delete_tp.UseVisualStyleBackColor = true;
            this.button_delete_tp.Click += new System.EventHandler(this.button_delete_tp_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1188, 786);
            this.Controls.Add(this.button_delete_tp);
            this.Controls.Add(this.button_add_tp);
            this.Controls.Add(this.button_red_tp);
            this.Controls.Add(this.button_delete_c);
            this.Controls.Add(this.button_red_c);
            this.Controls.Add(this.button_add_cont);
            this.Controls.Add(this.button_red_pr);
            this.Controls.Add(this.button_delete_pr);
            this.Controls.Add(this.button_delete_ab);
            this.Controls.Add(this.button_red_ab);
            this.Controls.Add(this.button_add_ab);
            this.Controls.Add(this.label);
            this.Controls.Add(this.ФИО);
            this.Controls.Add(this.FIO_Text);
            this.Controls.Add(this.find_phone);
            this.Controls.Add(this.main_tab_control);
            this.Controls.Add(this.button_add_pr);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.main_tab_control.ResumeLayout(false);
            this.name.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.abonent_t)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.contact_t)).EndInit();
            this.provaider.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.provider_t)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ab_tab)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_add_pr;
        private System.Windows.Forms.Button button_delete_ab;
        private System.Windows.Forms.Button button_red_ab;
        private System.Windows.Forms.Button button_add_ab;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label ФИО;
        private System.Windows.Forms.TextBox FIO_Text;
        private System.Windows.Forms.TextBox find_phone;
        private System.Windows.Forms.TabControl main_tab_control;
        private System.Windows.Forms.TabPage name;
        public System.Windows.Forms.DataGridView abonent_t;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView contact_t;
        private System.Windows.Forms.TabPage provaider;
        private System.Windows.Forms.DataGridView provider_t;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView ab_tab;
        private System.Windows.Forms.Button button_delete_pr;
        private System.Windows.Forms.Button button_red_pr;
        private System.Windows.Forms.Button button_add_cont;
        private System.Windows.Forms.Button button_red_c;
        private System.Windows.Forms.Button button_delete_c;
        private System.Windows.Forms.Button button_red_tp;
        private System.Windows.Forms.Button button_add_tp;
        private System.Windows.Forms.Button button_delete_tp;
    }
}

