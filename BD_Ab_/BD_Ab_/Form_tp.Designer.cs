﻿namespace BD_Ab_
{
    partial class Form_tp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button_OK = new System.Windows.Forms.Button();
            this.button_cancel = new System.Windows.Forms.Button();
            this.comboBox_phone = new System.Windows.Forms.ComboBox();
            this.comboBox_fio = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 17);
            this.label8.TabIndex = 79;
            this.label8.Text = "Телефон";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 70;
            this.label1.Text = "ФИО";
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(243, 119);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(62, 38);
            this.button_OK.TabIndex = 67;
            this.button_OK.Text = "Ок";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.button_OK_Click);
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(160, 119);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(67, 38);
            this.button_cancel.TabIndex = 66;
            this.button_cancel.Text = "Отмена";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // comboBox_phone
            // 
            this.comboBox_phone.FormattingEnabled = true;
            this.comboBox_phone.Location = new System.Drawing.Point(129, 60);
            this.comboBox_phone.Name = "comboBox_phone";
            this.comboBox_phone.Size = new System.Drawing.Size(176, 24);
            this.comboBox_phone.TabIndex = 86;
            // 
            // comboBox_fio
            // 
            this.comboBox_fio.FormattingEnabled = true;
            this.comboBox_fio.Location = new System.Drawing.Point(129, 13);
            this.comboBox_fio.Name = "comboBox_fio";
            this.comboBox_fio.Size = new System.Drawing.Size(176, 24);
            this.comboBox_fio.TabIndex = 87;
            // 
            // Form_tp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 168);
            this.Controls.Add(this.comboBox_fio);
            this.Controls.Add(this.comboBox_phone);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_cancel);
            this.Name = "Form_tp";
            this.Text = "Form_tp";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_OK;
        private System.Windows.Forms.Button button_cancel;
        public System.Windows.Forms.ComboBox comboBox_phone;
        public System.Windows.Forms.ComboBox comboBox_fio;
    }
}