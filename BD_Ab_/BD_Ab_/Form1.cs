﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace BD_Ab_
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
       private string connectionString = global::BD_Ab_.Properties.Settings.Default.Database1ConnectionString;
     //   string connectionString = string.Format("Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename={0};Integrated Security = True", GetConnectionSting());

       /* public static string GetConnectionSting()
        {
            var exeDirectory = Environment.CurrentDirectory;
            exeDirectory = exeDirectory.Substring(0, exeDirectory.Length - 10);
            var dbDirectory = "Db.mdf";
            return Path.Combine(exeDirectory, dbDirectory);
        }*/
      
        void updateAbonentDGV()
        {
            var request = "SELECT * FROM abonent";
            var adapter = new SqlDataAdapter(request, connectionString);
            var abonentTable = new DataTable();
            adapter.Fill(abonentTable);
            abonent_t.DataSource = abonentTable;
            abonent_t.Columns["id"].Visible = false;
            abonent_t.Columns["name"].HeaderText = "Имя";
            abonent_t.Columns["patronymic"].HeaderText = "Отчество";
            abonent_t.Columns["surname"].HeaderText = "Фамилия";
            abonent_t.Columns["adress"].HeaderText = "Адрес";
            abonent_t.Columns["birthdate"].HeaderText = "Дата рождения";
            abonent_t.Columns["comment"].HeaderText = "Комментарий";
            var exeDirectory = Environment.CurrentDirectory;
        }

        void updateContactDGV()
        {
            var request = "SELECT * FROM contact";
            var adapter = new SqlDataAdapter(request, connectionString);
            var contactTable = new DataTable();
            adapter.Fill(contactTable);
            contact_t.DataSource = contactTable;
            contact_t.Columns["id"].Visible = false;
            contact_t.Columns["phone"].HeaderText = "Телефон";
            contact_t.Columns["type"].HeaderText = "Тип";
            contact_t.Columns["provider_id"].Visible = false;
        }

        void updateProviderDGV()
        {
            var request = "SELECT * FROM provider";
            var adapter = new SqlDataAdapter(request, connectionString);
            var contactTable = new DataTable();
            adapter.Fill(contactTable);
            provider_t.DataSource = contactTable;
            provider_t.Columns["id"].Visible = false;
            provider_t.Columns["provider_company"].HeaderText = "Название";
            provider_t.Columns["score"].HeaderText = "Рейтинг";
        }

        void updatePhone_TabDGV()
        {
            var request =
          @"SELECT *FROM abonent JOIN 
            abonent_has_contact 
            ON abonent.id=abonent_has_contact.abonent_id 
            JOIN contact
            ON contact.id=abonent_has_contact.contact_id
            LEFT JOIN provider 
            ON provider.id=contact.provider_id ";


          

           if(FIO_Text.Text!="" || find_phone.Text !="")
            {
                request += @"WHERE (contact.phone LIKE+'%" + find_phone.Text + "%') AND (abonent.surname + ' '  + abonent.patronymic + ' '+ abonent.name LIKE + '%" + FIO_Text.Text + "%')";
            }

            var adapter = new SqlDataAdapter(request, connectionString);
            var Phone_Table = new DataTable();
            adapter.Fill(Phone_Table);



            ab_tab.DataSource = Phone_Table;
            ab_tab.Columns["id"].Visible = false;
            ab_tab.Columns["id1"].Visible = false;
            ab_tab.Columns["id2"].Visible = false;
            ab_tab.Columns["provider_id"].Visible = false;
            ab_tab.Columns["abonent_id"].Visible = false;
            ab_tab.Columns["contact_id"].Visible = false;
            ab_tab.Columns["birthdate"].Visible = false;
            ab_tab.Columns["score"].Visible = false;
            ab_tab.Columns["name"].HeaderText = "Имя";
            ab_tab.Columns["provider_company"].HeaderText = "Провайдер";
            ab_tab.Columns["patronymic"].HeaderText = "Отчество";
            ab_tab.Columns["surname"].HeaderText = "Фамилия";
            ab_tab.Columns["adress"].HeaderText = "Адрес";
            ab_tab.Columns["phone"].HeaderText = "Телефон";
            ab_tab.Columns["type"].HeaderText = "Тип";
            ab_tab.Columns["comment"].HeaderText = "Комментарий";
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            updateAbonentDGV();
            updateContactDGV();
            updateProviderDGV();
            updatePhone_TabDGV();
        }

        private void button_add_ab_Click(object sender, EventArgs e)//кнопка "добавить абонента"
        {
            var form = new Form_ab();
            {
                var request = "SELECT * FROM abonent";
                var adapter = new SqlDataAdapter(request, connectionString);
                var prTable = new DataTable();
                adapter.Fill(prTable);
            }
                var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var birthdate = Convert.ToDateTime(form.textBox_bd.Text).ToString("yyyy-MM-dd HH:mm:ss.fff");
                var surname = form.textBox_surname.Text;
                var name = form.textBox_name.Text;
                var patronymic = form.textBox_patronymic.Text;
                var address = form.textBox_adres.Text;
                var comment = form.textBox_comment.Text;
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var req= @"INSERT INTO abonent ( surname,name, patronymic, adress, comment, birthdate) VALUES 
                                                      ( '" + surname+"','"+ name+"','"+ patronymic+"','"+ address+"','"+ comment+"', '"+birthdate+"')";

                var command = new SqlCommand(req, connection);
                command.ExecuteNonQuery();
                connection.Close();


                updateAbonentDGV();
               
            }
        }
    

        private void button_red_ab_Click(object sender, EventArgs e)//кнопка "редактировать абонента"
        {


            var row = abonent_t.SelectedRows.Count > 0 ? abonent_t.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show(" Сначала выберите строчку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }
            var form = new Form_ab();
            form.textBox_surname.Text = row.Cells["surname"].Value.ToString();
            form.textBox_name.Text = row.Cells["name"].Value.ToString();
            form.textBox_patronymic.Text = row.Cells["patronymic"].Value.ToString();
            form.textBox_adres.Text = row.Cells["adress"].Value.ToString();
            form.textBox_comment.Text = row.Cells["comment"].Value.ToString();
            form.textBox_bd.Text = row.Cells["birthdate"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var birthdate = Convert.ToDateTime(form.textBox_bd.Text).ToString("yyyy-MM-dd HH:mm:ss.fff");
                var surname = form.textBox_surname.Text;
                var name = form.textBox_name.Text;
                var patronymic = form.textBox_patronymic.Text;
                var address = form.textBox_adres.Text;
                var comment = form.textBox_comment.Text;
                var id = row.Cells["id"].Value.ToString();
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var request = @"UPDATE  abonent SET surname='" + surname + "'," +
                    "name='" + name + "', patronymic='" + patronymic + "', adress= '" + address + "', comment='" + comment + "', birthdate='"+birthdate+"' WHERE id=" + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateAbonentDGV();
            }
        }

        private void button_delete_ab_Click(object sender, EventArgs e)//кнопка "удалить"
        {
            var dgrow = abonent_t.SelectedRows.Count > 0 ? abonent_t.SelectedRows[0] : null;
            if (dgrow == null)
            {
                MessageBox.Show(" Сначала выберите строчку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = dgrow.Cells["id"].Value.ToString();
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var request = @"DELETE  FROM  abonent  WHERE id=" + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            updateAbonentDGV();
        }

        private void find_phone_TextChanged(object sender, EventArgs e)
        {
            updatePhone_TabDGV();
        }

        private void FIO_Text_TextChanged(object sender, EventArgs e)
        {
            updatePhone_TabDGV();
        }

        private void button_add_pr_Click(object sender, EventArgs e)//кнопка "Добавить провайдера"
        {
            var form = new Form_pr();
            {
                var request = "SELECT * FROM provider";
                var adapter = new SqlDataAdapter(request, connectionString);
                var prTable = new DataTable();
                adapter.Fill(prTable);
            }
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var name_pr = form.textBox_name_pr.Text;
                var score = form.textBox_score.Text;
            
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var req = @"INSERT INTO provider ( provider_company, score) VALUES 
                                                      ( '" + name_pr + "','" + score + "')";

                var command = new SqlCommand(req, connection);
                command.ExecuteNonQuery();
                connection.Close();


                updateProviderDGV();

            }
        }

        private void button_red_pr_Click(object sender, EventArgs e)//кнопка "Редактировать провайдера"
        {
            var row = provider_t.SelectedRows.Count > 0 ? provider_t.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show(" Сначала выберите строчку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }
            var form = new Form_pr();
            form.textBox_name_pr.Text = row.Cells["provider_company"].Value.ToString();
            form.textBox_score.Text = row.Cells["score"].Value.ToString();
          
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var id = row.Cells["id"].Value.ToString();
                var name_pr = form.textBox_name_pr.Text;
                var score = form.textBox_score.Text;
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var request = @"UPDATE  provider SET provider_company='" + name_pr + "'," +
                    "score='" +score + "' WHERE id=" + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateProviderDGV();
            }
        }

        private void button_delete_pr_Click(object sender, EventArgs e)//кнопка "Удалить провайдера"
        {
            var dgrow = provider_t.SelectedRows.Count > 0 ? provider_t.SelectedRows[0] : null;
            if (dgrow == null)
            {
                MessageBox.Show(" Сначала выберите строчку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = dgrow.Cells["id"].Value.ToString();
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var request = @"DELETE  FROM  provider  WHERE id=" + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            updateProviderDGV();
        }
        private void button_add_cont_Click(object sender, EventArgs e)//кнопка "Добавить контакт"
        {
            var form = new Form_cont();
            {
                var request = "SELECT * FROM provider";
                var adapter = new SqlDataAdapter(request, connectionString);
                var prTable = new DataTable();
                adapter.Fill(prTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow row in prTable.Rows)
                {
                    dict.Add((int)row["id"], row["provider_company"].ToString());
                }
                form.ProviderData = dict;
            
                List<string> qwe = new List<string>();

                request = "SELECT DISTINCT  type From contact";
                adapter = new SqlDataAdapter(request, connectionString);
                prTable = new DataTable();
                adapter.Fill(prTable);

                foreach (DataRow row in prTable.Rows)
                {
                    qwe.Add(row["type"].ToString());
                }
                form.TypeData = qwe;

            }
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.textBox_phone.Text;
                var providerId = form.ProviderId;
                var type = form.comboBox_type.Text;
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var req = @"INSERT INTO contact (phone, type, provider_id) VALUES ('"+phone+"','"+ type+"','"+ providerId+"')";
                var command = new SqlCommand(req, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateContactDGV();
            }




        }

        private void button_red_c_Click(object sender, EventArgs e)//кнопка "Редактировать контакт"
        {
            var dgrow = contact_t.SelectedRows.Count > 0 ? contact_t.SelectedRows[0] : null;
            if (dgrow == null)
            {
                MessageBox.Show(" Сначала выберите строчку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var form = new Form_cont();
            form.textBox_phone.Text = dgrow.Cells["phone"].Value.ToString();
          
            {
                var request = "SELECT * FROM provider";
                var adapter = new SqlDataAdapter(request, connectionString);
                var prTable = new DataTable();
                adapter.Fill(prTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow row in prTable.Rows)
                {
                    dict.Add((int)row["id"], row["provider_company"].ToString());
                }
                form.ProviderData = dict;

                List<string> qwe = new List<string>();

                request = "SELECT DISTINCT  type From contact";
                adapter = new SqlDataAdapter(request, connectionString);
                prTable = new DataTable();
                adapter.Fill(prTable);

                foreach (DataRow row in prTable.Rows)
                {
                    qwe.Add(row["type"].ToString());
                }
                form.TypeData = qwe;
            }
            form.ProviderId = (int)dgrow.Cells["provider_id"].Value;

            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var id = dgrow.Cells["id"].Value.ToString();
                var phone = form.textBox_phone.Text;
                var providerId = form.ProviderId;
                var type = form.comboBox_type.Text;
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var req = @"UPDATE contact SET phone ='" +phone+ "'," +"type='" +type + "',"+"provider_id='"+providerId+"'WHERE id=" + id + "";
            
                var command = new SqlCommand(req, connection);
             
                command.ExecuteNonQuery();
                connection.Close();
                updateContactDGV();
            }


        }

        private void button_delete_c_Click(object sender, EventArgs e)//кнока "Удалить контакт"
        {
            var dgrow = contact_t.SelectedRows.Count > 0 ? contact_t.SelectedRows[0] : null;
            if (dgrow == null)
            {
                MessageBox.Show(" Сначала выберите строчку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = dgrow.Cells["id"].Value.ToString();
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var request = @"DELETE  FROM  contact  WHERE id=" + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            updateContactDGV();
        }

        private void button_add_tp_Click(object sender, EventArgs e)
        {
            var form = new Form_tp();
            {
                var request = "SELECT Id, surname, name, patronymic FROM abonent";
                var adapter = new SqlDataAdapter(request, connectionString);
                var abTable = new DataTable();
                adapter.Fill(abTable);
                var dict = new Dictionary<int, string>();
               

                foreach (DataRow row in abTable.Rows)
                {
                    dict.Add((int)row["id"], (row["surname"].ToString()+" "+ row["name"].ToString() + row["patronymic"].ToString()));
                }
                form.FioData = dict;
            }

            {
                var request = "SELECT Id, phone FROM contact";
                var adapter = new SqlDataAdapter(request, connectionString);
                var prTable = new DataTable();
                adapter.Fill(prTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow row in prTable.Rows)
                {
                    dict.Add((int)row["id"], (row["phone"].ToString()));
                }
                form.PhoneData = dict; 
            }

           
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var fio  = form.comboBox_fio.Text;
                var phone = form.comboBox_phone.Text;
                var PhoneId = form.PhoneId;             
                var FioId = form.FioId;
               
                var connection = new SqlConnection(connectionString);
                connection.Open();

               var req = @"INSERT INTO abonent_has_contact (abonent_id, contact_id) VALUES ('" +FioId.ToString() + "','" + PhoneId + "')";
                var command = new SqlCommand(req, connection);
                command.ExecuteNonQuery();
              
             
                connection.Close();
                
                updatePhone_TabDGV();
            }
        }

        private void button_red_tp_Click(object sender, EventArgs e)
        {
            var drow = ab_tab.SelectedRows.Count > 0 ? ab_tab.SelectedRows[0] : null;
            if (drow == null)
            {
                MessageBox.Show(" Сначала выберите строчку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var form = new Form_tp();
           
            {
                var request = "SELECT * FROM abonent";
                var adapter = new SqlDataAdapter(request, connectionString);
                var abTable = new DataTable();
                adapter.Fill(abTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow row in abTable.Rows)
                {
                    dict.Add((int)row["id"], (row["surname"].ToString() + " " + row["name"].ToString() + row["patronymic"].ToString()));
                }
                form.FioData = dict;
              
            }

            {
                var request = "SELECT * FROM contact";
                var adapter = new SqlDataAdapter(request, connectionString);
                var prTable = new DataTable();
                adapter.Fill(prTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow row in prTable.Rows)
                {
                    dict.Add((int)row["id"], (row["phone"].ToString()));
                }
                form.PhoneData = dict;
            }



           form.PhoneId = (int)drow.Cells["contact_id"].Value; 
            form.FioId = (int) drow.Cells["abonent_id"].Value;

            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var id_a = drow.Cells["abonent_id"].Value.ToString();
                var id_c = drow.Cells["contact_id"].Value.ToString();
                var abId = form.FioId;
                var contId = form.PhoneId;
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var req = @"UPDATE abonent_has_contact SET abonent_id ='" + abId + "'," + "contact_id='" + contId + "'WHERE abonent_id=" + id_a + " AND contact_id="+ id_c+"";

                var command = new SqlCommand(req, connection);

                command.ExecuteNonQuery();
                connection.Close();
                updatePhone_TabDGV();

            }
        }
        

        private void button_delete_tp_Click(object sender, EventArgs e)
        {
            var row_ = ab_tab.SelectedRows.Count > 0 ? ab_tab.SelectedRows[0] : null;
            if (row_ == null)
            {
                MessageBox.Show(" Сначала выберите строчку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
             var id_a = row_.Cells["abonent_id"].Value.ToString();
                var id_c = row_.Cells["contact_id"].Value.ToString();
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var request = @"DELETE  FROM  abonent_has_contact WHERE abonent_id=" + id_a + " AND contact_id = " + id_c + "; ";
           
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            updatePhone_TabDGV();
        }
    }
    
}
